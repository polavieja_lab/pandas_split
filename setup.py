from setuptools import setup

setup(
    name="pandas_split",
    version="0.0.1",
    description="pandas_split",
    url="https://gitlab.com/fjhheras/pandas_split",
    author="none",
    author_email="none",
    license="GPL",
    packages=["pandas_split"],
    #scripts=["bin/fasanalyse_experiment"],
    zip_safe=False,
)
