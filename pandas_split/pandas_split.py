from typing import Callable, List, Dict
import pandas as pd
from pathlib import Path
from joblib import Parallel, delayed

NUM_PROCESSES = 10


def concat_df_with_common(df: pd.DataFrame, common_dict: Dict):
    # Ensure that df has meaningless indices(we do a reset_index() before the
    # call to this function)
    assert df.index.names == [None]
    common_df = pd.DataFrame([common_dict] * len(df))
    return common_df.merge(df, left_index=True, right_index=True)


def read_df_file(
    df_file: Path,
    common_dict: Dict,
    meaningful_common_idx: bool,
    filter_df: Callable = None,  # affects to raw df
    df_columns: List = None,  # affects to the concatenated df
):
    df = pd.read_pickle(df_file)

    # Get Dataframe index names
    df_idx_names = list(df.index.names)
    if df_idx_names == [None]:
        # Meaningless DataFrame index (list of integers)
        meaningful_idx = False
        df = df.reset_index(drop=True)
    else:
        # The dataset index has some meaning in the dataset
        meaningful_idx = True
        df = df.reset_index()
    # We only consider the case where both the index_df and the df have the
    # same type of idx, either meaningful or a list of integers
    assert meaningful_common_idx == meaningful_idx

    # Filter rows
    if filter_df is not None:
        df = df[filter_df(df)]

    # Concatenate common dictionary with Dataframe
    df = concat_df_with_common(df, common_dict)

    if meaningful_idx:
        # Set index with columns that uniquely identify the data point (row)
        # df Dataframe indices were already meaningful for the dataset
        # so we output a df with meaningful indices
        common_dict_idx_names = list(common_dict.keys())
        df.set_index(common_dict_idx_names + df_idx_names, inplace=True)

    if df_columns is not None:
        # Will only work if all elements of `df_columns` are
        # columns of the `index_df` or columns of the mini dataframes.
        df = df[df_columns]

    return df


def read_all(
    path: str,
    filter_common: Callable = None,
    filter_df: Callable = None,
    df_columns: List = None,
):
    index_df = pd.read_pickle(Path(path) / "index.pkl")
    # Get name of Dataframe index
    if list(index_df.index.names) != [None]:
        # The Dataframe indices are not a list of integers and the name
        # of the indices is meaningful in the context of the dataset
        index_df.reset_index(inplace=True)
        meaningful_common_idx = True
    else:
        meaningful_common_idx = False

    # Filter index_df rows according to filter_common function
    if filter_common is not None:
        index_df = index_df[filter_common(index_df)]
        if index_df.empty:
            print("filter_common returned an empty dataframe")

    arguments = []
    for idx, row in index_df.iterrows():
        row_dict = dict(row)
        file_name = row_dict.pop("file")

        arguments.append(
            (
                Path(path) / file_name,
                row_dict,
                meaningful_common_idx,
                filter_df,
                df_columns,
            )
        )
    print("Reading data...")
    df_list = Parallel(n_jobs=8)(
        delayed(read_df_file)(*args) for args in arguments
    )
    if df_list:
        print("Concatenating...")
        return pd.concat(df_list)
    else:
        print(
            "WARGNING: No data found. "
            "Maybe the filter_common returned an empty Dataframe"
        )
        return pd.DataFrame()


def save_file(save_path, group, idx, common_idx, df_idx):
    common_dict = {col: idx_ for (col, idx_) in zip(common_idx, idx)}
    file_name = (
        ".".join(
            [
                "_".join([str(col), str(idx_)])
                for (col, idx_) in zip(common_idx, idx)
            ]
        )
        + ".pkl"
    )
    common_dict["file"] = file_name
    group.set_index(list(df_idx), inplace=True)
    group.drop(common_idx, axis=1, inplace=True)
    group.to_pickle(Path(save_path) / file_name)
    return common_dict


def save_split(
    save_path: str,
    data: pd.DataFrame,
    common_idx: List,
    df_idx: List,
):
    data_idx = data.index.names
    if data_idx != [None]:
        data.reset_index(inplace=True)
    groups = data.groupby(list(common_idx))
    common_dict_list = Parallel(n_jobs=8)(
        delayed(save_file)(save_path, group, idx, common_idx, df_idx)
        for idx, group in groups
    )

    return common_dict_list
